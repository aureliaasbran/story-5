from django import forms

class MatkulForm(forms.Form):
    nama_matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Matkul',
        'type': 'text',
        'required': True,
    }))

    dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Dosen Pengajar',
        'type': 'text',
        'required': True,
    }))

    jumlah_sks = forms.IntegerField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jumlah SKS',
        'type': 'integer',
        'required': True,
    }))

    deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi',
        'type': 'text',
        'required': True,
    }))

    semester = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Semester',
        'type': 'text',
        'required': True,
    }))

    ruangan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Ruangan',
        'type': 'text',
        'required': True,
    }))

