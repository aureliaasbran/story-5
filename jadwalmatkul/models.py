from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    jumlah_sks = models.IntegerField(blank=False)
    deskripsi = models.CharField(max_length=100)
    semester = models.CharField(max_length=50)
    ruangan = models.CharField(max_length=50)