from django.urls import path
from . import views
from .views import Add, matkul_delete, matkul_see

appname = 'jadwalmatkul'

urlpatterns = [
   path('', Add, name = 'Add'),
   path('/matkul', views.matkulpage, name='matkulpage'),
   path('see/delete/<int:pk>', matkul_delete, name='Delete'),
   path('see/matkul/<int:pk>', matkul_see, name='See'),
]
