from django.shortcuts import render,redirect
from .models import Matkul as jadwal
from .forms import MatkulForm

# Create your views here.

def matkulpage(request):
    return render(request, 'jadwalmatkul/matkulpage.html')

def Add(request):
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            matkul = jadwal()
            matkul.nama_matkul = form.cleaned_data['nama_matkul']
            matkul.dosen = form.cleaned_data['dosen']
            matkul.jumlah_sks = form.cleaned_data['jumlah_sks']
            matkul.deskripsi = form.cleaned_data['deskripsi']
            matkul.semester = form.cleaned_data['semester']
            matkul.ruangan = form.cleaned_data['ruangan']
            matkul.save()
        return redirect('/matkul')
    else:
        matkul = jadwal.objects.all()
        form = MatkulForm()
        response = {"matkul":matkul, 'form':form}
        return render (request, 'matkulpage.html', response)

def matkul_delete(request, pk):
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            matkul = jadwal()
            matkul.nama_matkul = form.cleaned_data['nama_matkul']
            matkul.dosen = form.cleaned_data['dosen']
            matkul.jumlah_sks = form.cleaned_data['jumlah_sks']
            matkul.deskripsi = form.cleaned_data['deskripsi']
            matkul.semester = form.cleaned_data['semester']
            matkul.ruangan = form.cleaned_data['ruangan']
            matkul.save()
        return redirect('/matkul')
    else:
        matkul.objects.filter(pk=pk).delete()
        data = matkul.objects.all()
        form = MatkulForm()
        response = {"matkul":data, 'form':form}
        return render(request, 'matkulpage.html', response)

def matkul_see(request):
    if request.method == "POST":
        pk = request.POST['pk']
        matkul = jadwal.objects.get(pk=pk)
        response={"matkul":data}
        return render(request, 'lihatmatkul.html', response)
