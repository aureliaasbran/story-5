from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('/education', views.educationpage, name='educationpage'),
    path('/family', views.familypage, name='familypage'),
    path('/interests', views.interestspage, name='interestspage'),
    path('/contacts', views.contactpage, name='contactpage'),
]
